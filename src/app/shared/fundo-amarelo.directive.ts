import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appFundoAmarelo]'
  // selector: 'p, button [appFundoAmarelo]'
})
export class FundoAmareloDirective {

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    // console.log(this.elementRef);
    // this.elementRef.nativeElement.style.backgroundColor = 'yellow'; //Má prática (Causa vulnerabilidade)
    this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'yellow'); //Boa práticas
  }

}
